﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Plane_Brain : MonoBehaviour
{
    public int start_money;

    public Text kills;
    public Text timeNum;
    public Text spawnCount;
    public Text moneyCount;
    public Text turretCount;
    public int turretCost;

    private int k_count;
    public float time_count;
    private int spawn_count;
    private int money_count;

    public GameObject objectToinstantiate;
    

    // Use this for initialization
    void Start()
    {
        spawnCount.text = ""; //add spawn number or goal

    time_count = Time.time;
        timeNum.text = Math.Round(time_count, 2).ToString();

        turretCount.text = turretCost.ToString();

        money_count = start_money;
        moneyCount.text = money_count.ToString();
            

        k_count = 0;
        kills.text = "Kills Count: " + k_count.ToString();

        //need code with spawner to count enemies     
        
        
    }
       
    void Update()
    {
        
        //KEEP UPDATING THE GAME TIME
        time_count = Time.time;
        timeNum.text = Math.Round(time_count, 2).ToString();
        


        Ray myRay;      // initializing the ray
        RaycastHit hit; // initializing the raycasthit
        myRay = Camera.main.ScreenPointToRay(Input.mousePosition); // telling my ray variable that the ray will go from the center of 
        
            if (Physics.Raycast(myRay, out hit))
            { // here I ask : if myRay hits something, store all the info you can find in the raycasthit varible.
                if (Input.GetMouseButtonDown(0))
                {// what to do if i press the left mouse button
                
                    if (money_count >= turretCost)
                    {
                        Instantiate(objectToinstantiate, hit.point, Quaternion.identity);// instatiate a prefab on the position where the ray hits the floor.
                        Debug.Log(hit.point);// debugs the vector3 of the position where I clicked

                        money_count -= turretCost;
                        moneyCount.text = money_count.ToString();
                    }
                    else
                    {
                     moneyCount.color = Color.red;
                    }
                }// end upMousebutton
            }// end physics.raycast

            
        
    }// end Update method


    public void KillCountUp()
    {
        Debug.Log("Kill Called");
        
        k_count++;
        kills.text = "Kills Count: " + k_count.ToString();

        money_count += 2;
        moneyCount.text = money_count.ToString();

        moneyCount.color = Color.yellow;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }
}