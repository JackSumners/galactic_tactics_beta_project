﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameOver : MonoBehaviour
{
    public Text timerText;
    //public Plane_Brain time_brain;
    public string t;

    private void Start()
    {
       t = Math.Round(Time.time, 2).ToString();
    }
    private void Update()
    {
        timerText.text = t;
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Beta_Scene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}